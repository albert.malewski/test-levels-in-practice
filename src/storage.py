from typing import Optional

from models.users import User


class UserStorage:
    __users = []

    @classmethod
    def add(cls, user: User) -> None:
        cls.__users.append(user)

    @classmethod
    def find(cls, user_id) -> Optional[User]:
        return next((u for u in cls.__users if u.user_id == user_id), None)

    @classmethod
    def remove(cls, user: User):
        cls.__users.remove(user)

    @classmethod
    def to_list(cls) -> []:
        return [
            user.to_dict() for user in cls.__users
        ]
