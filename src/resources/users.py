from flask import request
from flask_restful import Resource

import config
from connectors import KafkaConnector
from models.users import User
from storage import UserStorage

users_storage = UserStorage


class UserListController(Resource):
    kafka_connector = KafkaConnector

    def get(self):
        """Returns a list of all users"""
        return users_storage.to_list(), 200

    def post(self):
        """Creates new user"""
        new_user = User(
            request.json['email'],
        )
        users_storage.add(new_user)
        self.kafka_connector.send_to_topic(config.kafka_topic_users_updates, new_user.to_dict(), new_user.user_id)
        return new_user.to_dict(), 201


class UserController(Resource):
    def get(self, user_id):
        """Returns details of a user"""
        user = users_storage.find(user_id)
        return user.to_dict()
